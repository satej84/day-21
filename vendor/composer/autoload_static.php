<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit4ad5fe4b85017c48b0af309df3b5113d
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit4ad5fe4b85017c48b0af309df3b5113d::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit4ad5fe4b85017c48b0af309df3b5113d::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
